# Reactive Rows

An extension to SQLite4Unity3d
https://github.com/codecoding/SQLite4Unity3d

Adds the ability to subscribe to row and table modifications.

(i.e. a monster could subscribe to its own row in the db,  or a UI element could subscribe to changes in the number of rows in the monster table)