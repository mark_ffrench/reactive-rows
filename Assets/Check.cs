﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helpers
{
    /// <summary>
    /// Provides quick one-line asserts for multiple arguments passed into a method
    /// This intended to provide the opening check of arguments for Design by Contract
    /// 
    /// Doing the same with Assert would require one line for each argument, which is too verbose
    /// I couldn't extend Assert, since its a static class, so I created this new one
    /// </summary>
    public static class Check
    {
        /// <summary>
        /// Checks that a series of arguments are not null
        /// </summary>
        /// <param name="args">Pass as many nullable objects in here as you like</param>
        public static void NotNull(params object[] args)
        {
            int count = 0;
            foreach(object o in args)
            {
                if (o == null)
                    throw new ArgumentNullException(string.Format("Argument {0} was null", count));

                count++;
            }
        }

        /// <summary>
        /// Checks that a series of strings are valid
        /// </summary>
        /// <param name="args">A series of string arguments</param>
        public static void StringNotNullOrEmpty(params string[] args)
        {
            int count = 0;
            foreach (string target in args)
            {
                if (string.IsNullOrEmpty(target))
                {
                    throw new ArgumentException(string.Format("Argument {0} was a null or empty string.", count));
                }
                count++;
            }
        }
    }
}
