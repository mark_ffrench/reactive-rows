using System;
using System.Collections;
using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Helpers;

public abstract class DataService : ScriptableObject
{
    public SQLiteConnection _connection;
    public string DBFileName = "game.db";
    
    public void CreateDB()
    {
        string directoryName = "Assets/StreamingAssets";
        Directory.CreateDirectory(directoryName);
        
#if UNITY_EDITOR
        var dbPath = string.Format(@"{0}/{1}", directoryName, DBFileName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
             var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#elif UNITY_WP8
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);

#elif UNITY_WINRT
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#else
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
        _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        //Debug.Log("Final PATH: " + dbPath);
        onRowModifiedChannel = new Dictionary<Type, object>();
        onRowDeletedChannel = new Dictionary<Type, object>();
        onTableInsertionChannel = new Dictionary<Type, object>();
        onTableRemovalChannel = new Dictionary<Type, object>();
        OnCreate();
    }


    protected abstract void OnCreate();


    protected void CreateTable<T>()
    {
        _connection.DropTable<T>();
        _connection.CreateTable<T>();
    }

    protected void CreateTable<T>(T[] rows)
    {
        _connection.DropTable<T>();
        _connection.CreateTable<T>();
        _connection.InsertAll(rows);
    }

    protected IEnumerable<T> GetTable<T>() where T : new()
    {
        return _connection.Table<T>();
    }

    protected T Get<T>(object pk) where T : new()
    {
        return _connection.Get<T>(pk);
    }

    /// <summary>
    /// Modifies a db object, and immediately saves the change.
    /// </summary>
    /// <typeparam name="T">DB type</typeparam>
    /// <param name="id">The primary key</param>
    /// <param name="modify">Action to be performed on the object</param>
    /// <returns>The object you just modified</returns>
    protected T Modify<T>(object id, Action<T> modify) where T : new()
    {
        T obj = _connection.Get<T>(id);
        modify(obj);
        _connection.Update(obj);
        notifyChangeSubscribers<T>(id, obj);
        notifyTableModificationSubscribers<T>(obj);
        return obj;
    }

    protected void Modify<T>(object id, T obj) where T : new()
    {
        _connection.Update(obj);
        notifyChangeSubscribers<T>(id, obj);
        notifyTableModificationSubscribers<T>(obj);
    }
    
    protected void DeleteOrUpdate<T>(object id, T obj, bool delete) where T : new()
    {
        if (delete)
        {
            _connection.Delete(obj);
            notifyDeleteSubscribers<T>(id, obj);
            notifyTableRemovalSubscribers<T>(obj);
            
            removeSubscribers<T>(id, onRowDeletedChannel);
            removeSubscribers<T>(id, onRowModifiedChannel);
        }
        else
        {
            _connection.Update(obj);
            notifyChangeSubscribers<T>(id, obj);
        }
    }

    protected void Delete<T>(object id) where T : new()
    {
        T obj = _connection.Get<T>(id);
        _connection.Delete<T>(id);
        notifyDeleteSubscribers<T>(id, obj);
        notifyTableRemovalSubscribers<T>(obj);
        
        removeSubscribers<T>(id, onRowDeletedChannel);
        removeSubscribers<T>(id, onRowModifiedChannel);
    }
      
    protected void insert<T>(T obj) where T : new()
    {
        _connection.Insert(obj);
        notifyTableInsertSubscribers(obj);
    }

    #region DataBinding
    
    //these channels fire if something happens to any row in the table
    private Dictionary<Type, object> onTableModifiedChannel = new Dictionary<Type, object>();
    private Dictionary<Type, object> onTableRemovalChannel = new Dictionary<Type, object>();
    private Dictionary<Type, object> onTableInsertionChannel = new Dictionary<Type, object>();
    
    //these channels fire if something happens to the row you've subscribed to
    private Dictionary<Type, object> onRowModifiedChannel = new Dictionary<Type, object>();
    private Dictionary<Type, object> onRowDeletedChannel = new Dictionary<Type, object>();
    
    private void notifyChangeSubscribers<T>(object id, T obj) where T : new()
    {
        notifyChannel(id, obj, onRowModifiedChannel);
    }

    private void notifyDeleteSubscribers<T>(object id, T obj) where T : new()
    {
        notifyChannel(id, obj, onRowDeletedChannel);
    }
    
    private void notifyTableModificationSubscribers<T>(T obj) where T : new()
    {
        notifyTableChannel(obj, onTableModifiedChannel);
    }
    
    private void notifyTableRemovalSubscribers<T>(T obj) where T : new()
    {
        notifyTableChannel(obj, onTableRemovalChannel);
    }
    
    private void notifyTableInsertSubscribers<T>(T obj) where T : new()
    {
        notifyTableChannel(obj, onTableInsertionChannel);
    }

    private void notifyTableChannel<T>(T obj, Dictionary<Type, object> channel) where T : new()
    {
        object typeCallbacks;

        channel.TryGetValue(typeof(T), out typeCallbacks);

        if (typeCallbacks != null)
        {
            List<Action<T>> actions = (List<Action<T>>) typeCallbacks;

            foreach (Action<T> action in actions)
            {
                if (action != null)
                    action.Invoke(obj);                
            }
        }
        else
        {
            //Debug.LogError("no table callback list found for " + typeof(T));
        }
    }
    
    private void notifyChannel<T>(object id, T obj, Dictionary<Type, object> channel) where T : new()
    {
        object typeCallbacks;

        channel.TryGetValue(typeof(T), out typeCallbacks);

        if (typeCallbacks != null)
        {
            Dictionary<object, Action<T>> actions = (Dictionary<object, Action<T>>) typeCallbacks;

            Action<T> action;
            actions.TryGetValue(id, out action);

            if (action != null)
                action.Invoke(obj);
        }
        else
        {
            //Debug.LogError("no callback dictionary found for " + typeof(T));
        }
    }

    
    public void SubscribeToRowChanges<T>(object id, Action<T> onChanged) where T : new()
    {
        subscribeToChannel(id, onChanged, onRowModifiedChannel);
    }    
    
    public void SubscribeToRowDeletion<T>(object id, Action<T> onDeleted) where T : new()
    {
        subscribeToChannel(id, onDeleted, onRowDeletedChannel);
    }

    public void SubscribeToNewRows<T>(Action<T> onInserted) where T : new()
    {
        subscribeToTable(onInserted, onTableInsertionChannel);        
    }

    public void SubscribeToRemovedRows<T>(Action<T> onRemoved) where T : new()
    {
        subscribeToTable(onRemoved, onTableRemovalChannel);        
    }
    
    public void SubscribeToModifiedRows<T>(Action<T> onModified) where T : new()
    {
        subscribeToTable(onModified, onTableModifiedChannel);        
    }
    
    private void subscribeToTable<T>(Action<T> callback, Dictionary<Type, object> channel) where T : new()
    {
        if(!channel.ContainsKey(typeof(T)))
        {
            channel.Add(typeof(T), new List<Action<T>>());
        }

        object typeCallbacks;

        channel.TryGetValue((typeof(T)), out typeCallbacks);

        if (typeCallbacks != null)
        {
            List<Action<T>> actions = (List<Action<T>>) typeCallbacks;
            actions.Add(callback);
        }
    }
    
    private void subscribeToChannel<T>(object id, Action<T> onChanged, Dictionary<Type, object> channel) where T : new()
    {
        object typeCallbacks;

        if(!channel.ContainsKey(typeof(T)))
        {
            channel.Add(typeof(T), new Dictionary<object, Action<T>>());
        }
        
        channel.TryGetValue(typeof(T), out typeCallbacks);

        if (typeCallbacks != null)
        {
            Dictionary<object, Action<T>> actions = (Dictionary<object, Action<T>>) typeCallbacks;
            if (actions.ContainsKey(id))
            {
                actions[id] = onChanged;
            }
            else
            {
                actions.Add(id, onChanged);
            }
        }
        else
        {
            Debug.LogError("no callback dictionary found for " + typeof(T));
        }
    }

    private void removeSubscribers<T>(object id, Dictionary<Type, object> channel) where T : new()
    {
        object typeCallbacks;

        if(!channel.ContainsKey(typeof(T)))
        {
            channel.Add(typeof(T), new Dictionary<object, Action<T>>());
        }
        
        channel.TryGetValue(typeof(T), out typeCallbacks);

        if (typeCallbacks != null)
        {
            Dictionary<object, Action<T>> actions = (Dictionary<object, Action<T>>) typeCallbacks;
            if (actions.ContainsKey(id))
            {
                actions.Remove(id);
            }
        }
        else
        {
            Debug.LogError("no callback dictionary found for " + typeof(T));
        }
    }
    
    private void removeTableSubscribers<T>(Dictionary<Type, object> channel) where T : new()
    {
        if(!channel.ContainsKey(typeof(T)))
        {
            channel.Add(typeof(T), new List<Action<T>>());
        }

        object typeCallbacks;

        channel.TryGetValue((typeof(T)), out typeCallbacks);

        if (typeCallbacks != null)
        {
            List<Action<T>> actions = (List<Action<T>>) typeCallbacks;
            actions.Clear();
        }
    }
    
    #endregion //DataBinding
}
