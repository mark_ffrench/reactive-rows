﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerView : MonoBehaviour
{
    public GameDatabase database;
    private Text label;
    private int playerID;
    
    private void Start()
    {
        label = GetComponentInChildren<Text>();
        Player p = database.CreatePlayer(Display, OnDeleted);
        playerID = p.ID;
        Display(p);
    }

    private void OnDeleted(Player p)
    {
        Destroy(gameObject);
    }
    
    public void OnClick()
    {
        database.InjurePlayer(playerID, 10);
    }

    public void Display(Player p)
    {
        label.text = p.ID + Environment.NewLine + p.Health;
    }
    
    
}
