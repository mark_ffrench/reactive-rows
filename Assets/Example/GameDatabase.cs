﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class GameDatabase : DataService
{
    protected override void OnCreate()
    {
        CreateTable<Player>();
    }

    public Player CreatePlayer(Action<Player> onChanged, Action<Player> onDeleted)
    {
        Player p = new Player()
        {
            Health = 100
        };
        insert(p);

        if (onChanged != null)
            SubscribeToRowChanges(p.ID, onChanged);

        if (onDeleted != null)
            SubscribeToRowDeletion(p.ID, onDeleted);

        return p;
    }

    public void InjurePlayer(int id, int dmg)
    {
        Player player = Get<Player>(id);
        player.Health -= dmg;

        if (player.Health > 0)
        {
            Modify(player.ID, player);
        }
        else
        {
            Delete<Player>(id);
        }
    }
}