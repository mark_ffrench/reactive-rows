﻿using System;
using SQLite4Unity3d;
using UnityEngine;

[Serializable]
public class Player
{
    [SerializeField] private int id;
    [SerializeField] private int health;
    
    [PrimaryKey, AutoIncrement]
    public int ID { get { return id; } set { id = value; }}
    public int Health { get { return health; } set { health = value; }}
}
