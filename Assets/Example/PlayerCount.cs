﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCount : MonoBehaviour
{
	public GameDatabase database;
	private int count = 0;
	private Text label;
	
	// Use this for initialization
	void OnEnable ()
	{
		label = GetComponent<Text>();
		database.SubscribeToNewRows<Player>(onInserted);
		database.SubscribeToRemovedRows<Player>(onRemoved);
	}

	private void onRemoved(Player player)
	{
		count--;
		label.text = "Players: " + count;
	}

	private void onInserted(Player player)
	{
		count++;
		label.text = "Players: " + count;
	}
}
