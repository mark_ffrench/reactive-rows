﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastHit : MonoBehaviour {

    public GameDatabase database;
    private Text label;
	
    // Use this for initialization
    void OnEnable ()
    {
        label = GetComponent<Text>();
        database.SubscribeToModifiedRows<Player>(onModified);
        database.SubscribeToRemovedRows<Player>(onRemoved);
    }

    private void onRemoved(Player player)
    {
        label.text = "Player "+player.ID+" was killed!";
    }

    private void onModified(Player player)
    {
        label.text = "Player "+player.ID+" health is now "+player.Health;
    }
}
