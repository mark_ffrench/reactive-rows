﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DBWakerUpper : MonoBehaviour
{

	public UnityEvent OnDBReady;
	
	public DataService dataService;
	// Use this for initialization
	void Awake () {
		dataService.CreateDB();
		OnDBReady.Invoke();
	}

	private void OnDestroy()
	{
		//dataService.ClearSubscriptions();
	}
}
