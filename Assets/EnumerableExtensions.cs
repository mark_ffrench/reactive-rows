﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Helpers
{
	
    /// <summary>
    /// Provides a set of handy game-centric extensions for enumerable types
    /// </summary>
	public static class EnumerableExtension
	{
	    public static T PickRandom<T>(this IEnumerable<T> source)
	    {
	        return source.PickRandom(1).Single();
	    }
	
	    public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
	    {
	        return source.Shuffle().Take(count);
	    }
	
	    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
	    {
	        return source.OrderBy(x => Guid.NewGuid());
	    }
	
	
	    public static TValue GetValueOrDefault<TKey, TValue>
	    (this IDictionary<TKey, TValue> dictionary,
	     TKey key,
	     TValue defaultValue)
	    {
	        TValue value;
	        return dictionary.TryGetValue(key, out value) ? value : defaultValue;
	    }
        // 
        //     public static TValue GetValueOrNull<TKey, TValue>
        //     (this IDictionary<TKey, TValue> dictionary, TKey key)
        //     {
        //         TValue value;
        //         return dictionary.TryGetValue(key, out value) ? value : null;
        //     }

	    public static IEnumerable<TResult> LeftOuterJoin<TSource, TInner, TKey, TResult>(this IEnumerable<TSource> source, IEnumerable<TInner> other, Func<TSource, TKey> func, Func<TInner, TKey> innerkey, Func<TSource, TInner, TResult> res)
	    {
	        return from f in source
	            join b in other on func.Invoke(f) equals innerkey.Invoke(b) into g
	            from result in g.DefaultIfEmpty<TInner>()
	            select res.Invoke(f, result);
	    }
	}
}
