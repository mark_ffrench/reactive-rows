﻿using System;
using UnityEditor;
using UnityEngine;

namespace Helpers
{
    [CustomEditor(typeof(GameDatabase))]
    public class DataServiceInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GameDatabase dataService = (GameDatabase) target;


            if (GUILayout.Button("Create DB File"))
            {
                dataService.CreateDB();
            }
            
        }
    }
}